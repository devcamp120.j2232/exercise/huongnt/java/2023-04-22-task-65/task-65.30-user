package com.devcamp.userapi.service;

import java.util.ArrayList;
import java.util.Set;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.userapi.model.COrder;
import com.devcamp.userapi.model.CUser;
import com.devcamp.userapi.repository.IUserRepository;

@Service
public class UserService {
    @Autowired
    IUserRepository pIUserRepository;

    //get all
    public ArrayList<CUser> getUserList() {
        ArrayList<CUser> listUser = new ArrayList<>();
        pIUserRepository.findAll().forEach(listUser::add);
        return listUser;
    }

    //get by id
    public CUser getCountryById(long id) {
		if (pIUserRepository.findById(id).isPresent())
			return pIUserRepository.findById(id).get();
		else
			return null;
	}

    //create
    public CUser createUser(CUser cUser){
        try {
			CUser savedRole = pIUserRepository.save(cUser);
			return savedRole;
		} catch (Exception e) {
			return null;
		}
    }

    //update
    public CUser updateUser(long id, CUser cUser) {
		if (pIUserRepository.findById(id).isPresent()) {
			CUser newUser = pIUserRepository.findById(id).get();
			newUser.setFullname(cUser.getFullname());
			newUser.setAddress(cUser.getAddress());
			newUser.setEmail(cUser.getEmail());
            newUser.setPhone(cUser.getPhone());
			CUser savedUser = pIUserRepository.save(newUser);
			return savedUser;
		} else {
			return null;
		}
	}

	public Set<COrder> getOrderByCustomerId(Long id){
		Optional<CUser> vCustomer = pIUserRepository.findById(id);
		if (vCustomer != null){
			CUser newUser = pIUserRepository.findById(id).get();
			Set<COrder> savedCustomer = newUser.getOrders();
			return savedCustomer;
		}
		else return null;
	}


}
