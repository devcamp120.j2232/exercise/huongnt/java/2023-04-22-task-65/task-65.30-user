package com.devcamp.userapi.service;

import org.springframework.stereotype.Service;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import com.devcamp.userapi.model.COrder;
import com.devcamp.userapi.repository.IOrderRepository;

@Service
public class OrderService {
    @Autowired
    IOrderRepository pOrderRepository;

    // get all
    public ArrayList<COrder> getOrderList() {
        ArrayList<COrder> listOrder = new ArrayList<>();
        pOrderRepository.findAll().forEach(listOrder::add);
        return listOrder;
    }

    // get by id
    public COrder getOrderById(long id) {
        if (pOrderRepository.findById(id).isPresent())
            return pOrderRepository.findById(id).get();
        else
            return null;
    }

    // create
    public COrder createOrder(COrder cOrder) {
        try {
            COrder savedRole = pOrderRepository.save(cOrder);
            return savedRole;
        } catch (Exception e) {
            return null;
        }
    }

    //update
        public COrder updateOrder(long id, COrder cOrder) {
            if (pOrderRepository.findById(id).isPresent()) {
                COrder newOrder = pOrderRepository.findById(id).get();
                newOrder.setOrderCode(cOrder.getOrderCode());
                newOrder.setPizzaSize(cOrder.getPizzaSize());
                newOrder.setPizzaType(cOrder.getPizzaType());
                newOrder.setVoucherCode(cOrder.getVoucherCode());
                newOrder.setPrice(cOrder.getPrice());
                newOrder.setPaid(cOrder.getPaid());
                COrder savedOrder = pOrderRepository.save(newOrder);
                return savedOrder;
            } else {
                return null;
            }
        }

}
