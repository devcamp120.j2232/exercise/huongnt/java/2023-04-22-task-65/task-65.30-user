package com.devcamp.userapi.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.userapi.model.COrder;
import com.devcamp.userapi.model.CUser;
import com.devcamp.userapi.repository.IOrderRepository;
import com.devcamp.userapi.repository.IUserRepository;
import com.devcamp.userapi.service.OrderService;
import com.devcamp.userapi.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class OrderController {
    @Autowired
    OrderService pOrderService;

    // Lấy danh sách all orders dùng service.
    @GetMapping("/all-order") // Dùng phương thức GET
    public ResponseEntity<List<COrder>> getAllUserApi() {
        try {
            return new ResponseEntity<>(pOrderService.getOrderList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Autowired
    UserService pUserService;

    // get order by user id
    @GetMapping("/search/userId={id}")
    public ResponseEntity<Set<COrder>> getOrderByUserIdApi(@PathVariable(name = "id") long id) {
        try {
            return new ResponseEntity<>(pUserService.getOrderByCustomerId(id), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get order by id
    @GetMapping("/order/details/{id}")
    public COrder getOrderById(@PathVariable Long id) {
        return pOrderService.getOrderById(id);
    }

    // create new user with service
    @PostMapping("/order/create")
    // tạo với service
    public ResponseEntity<Object> createOrder1(@RequestBody COrder cOrder) {
        try {
            return new ResponseEntity<>(pOrderService.createOrder(cOrder), HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified order: " + e.getCause().getCause().getMessage());
        }
    }

        // update with service
        @PutMapping("/order/update/{id}")
        public ResponseEntity<Object> updateOrderById(@PathVariable("id") Long id, @RequestBody COrder cOrder) {
            try {
                return new ResponseEntity<>(pOrderService.updateOrder(id, cOrder), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }

        @Autowired
        IOrderRepository pOrderRepository;
        @DeleteMapping("/order/delete/{id}")
        public ResponseEntity<Object> deleteUserById(@PathVariable Long id) {
            try {
                pOrderRepository.deleteById(id);
                return new ResponseEntity<Object>("Deleted id  " +  id ,HttpStatus.OK);
            } catch (Exception e) {
                System.out.println(e);
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    
}
