package com.devcamp.userapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.userapi.model.CUser;
import com.devcamp.userapi.repository.IUserRepository;
import com.devcamp.userapi.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class UserController {
    @Autowired
    UserService pUserService;

    // Lấy danh sách all user dùng service.
    @GetMapping("/all-user") // Dùng phương thức GET
    public ResponseEntity<List<CUser>> getAllUserApi() {
        try {
            return new ResponseEntity<>(pUserService.getUserList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get user by id
    @GetMapping("/user/details/{id}")
    public CUser getCountryById(@PathVariable Long id) {
        return pUserService.getCountryById(id);
    }

    // create new user with service
    @PostMapping("/user/create")
    // tạo với service
    public ResponseEntity<Object> createUser1(@RequestBody CUser cUser) {
        try {
            return new ResponseEntity<>(pUserService.createUser(cUser), HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified user: " + e.getCause().getCause().getMessage());
        }
    }

    // update with service
    @PutMapping("/user/update/{id}")
    public ResponseEntity<Object> updateUserById(@PathVariable("id") Long id, @RequestBody CUser cUser) {
        try {
            return new ResponseEntity<>(pUserService.updateUser(id, cUser), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // delete without service
    @Autowired
    private IUserRepository pIUserRepository;

    @DeleteMapping("/user/delete/{id}")
    public ResponseEntity<Object> deleteUserById(@PathVariable Long id) {
        try {
            pIUserRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Viết method delete user: deleteUser(), và chạy test trên postman Chú ý:
    // delete user thì xóa luôn all orders thuộc user đó
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long id) {
        Optional<CUser> _userData = pIUserRepository.findById(id);
        // nếu khác null
        if (_userData.isPresent()) {
            try {
                pIUserRepository.deleteById(id);
                return new ResponseEntity<Object>("Deleted id  " +  id ,HttpStatus.OK);
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                // không thể thực thi hoạt động này
            }

        } else {
            return new ResponseEntity<Object>("User does not exist", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
